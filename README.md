# gokafka

[![GoReportCard](https://goreportcard.com/badge/gitlab.com/unboundsoftware/go-kafka)](https://goreportcard.com/report/gitlab.com/unboundsoftware/go-kafka) [![GoDoc](https://godoc.org/gitlab.com/unboundsoftware/go-kafka?status.svg)](https://godoc.org/gitlab.com/unboundsoftware/go-kafka) [![Build Status](https://gitlab.com/unboundsoftware/go-kafka/badges/master/pipeline.svg)](https://gitlab.com/unboundsoftware/go-kafka/commits/master)[![coverage report](https://gitlab.com/unboundsoftware/go-kafka/badges/master/coverage.svg)](https://gitlab.com/unboundsoftware/go-kafka/commits/master)



Download:
```shell
go get gitlab.com/unboundsoftware/kafka
```
