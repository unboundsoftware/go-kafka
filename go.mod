module gitlab.com/unboundsoftware/go-kafka

go 1.15

require (
	github.com/confluentinc/confluent-kafka-go v1.5.2 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/pkg/errors v0.9.1
	github.com/sanity-io/litter v1.3.0
	github.com/stretchr/testify v1.6.1 // indirect
	gopkg.in/check.v1 v1.0.0-20200902074654-038fdea0a05b // indirect
	gopkg.in/confluentinc/confluent-kafka-go.v1 v1.5.2
	gopkg.in/yaml.v3 v3.0.0-20200615113413-eeeca48fe776 // indirect
)
