package kafka

// Logger represents the logging API
// Maps to Apex log interface for convenience
// https://github.com/apex/log/blob/master/interface.go
type Logger interface {
	Debug(string)
	Info(string)
	Warn(string)
	Error(string)
	Fatal(string)
	Debugf(string, ...interface{})
	Infof(string, ...interface{})
	Warnf(string, ...interface{})
	Errorf(string, ...interface{})
	Fatalf(string, ...interface{})
}

type noopLogger struct {
}

func (m *noopLogger) Debug(s string) {
}

func (m *noopLogger) Info(s string) {
}

func (m *noopLogger) Warn(s string) {
}

func (m *noopLogger) Error(s string) {
}

func (m *noopLogger) Fatal(s string) {
}

func (m *noopLogger) Debugf(s string, i ...interface{}) {
}

func (m *noopLogger) Infof(s string, i ...interface{}) {
}

func (m *noopLogger) Warnf(s string, i ...interface{}) {
}

func (m *noopLogger) Errorf(s string, i ...interface{}) {
}

func (m *noopLogger) Fatalf(s string, i ...interface{}) {
}

var _ Logger = &noopLogger{}
